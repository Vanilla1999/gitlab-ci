
# Т.к. основным инструментом для сборки Android-проектов является Gradle, 
# и по счастливому стечению обстоятельств есть официальный Docker-образ 
# мы решили за основу взять именно его с нужной нам версией Gradle
FROM gradle:7.4.2-jdk17


# Задаем переменные с локальной папкой для Android SDK и 
# версиями платформы и инструментария
ENV SDK_URL="https://dl.google.com/android/repository/commandlinetools-linux-8092744_latest.zip" \
    ANDROID_HOME="/usr/local/android-sdk" \
	 ANDROID_HOME2="C:\Users\zavad\AppData\Local\Android\Sdk\cmdline-tools" \
    ANDROID_VERSION=30 \
    ANDROID_BUILD_TOOLS_VERSION=30.0.2

# Download Android SDK
# Создаем папку, скачиваем туда SDK и распаковываем архив,
# который после сборки удаляем
RUN mkdir "$ANDROID_HOME" .android \
    && cd "$ANDROID_HOME" \
    && curl -o sdk.zip $SDK_URL \
    && unzip sdk.zip \
    && rm sdk.zip \
# В следующих строчках мы создаем папку и текстовые файлы 
# с лицензиями. На оф. сайте Android написано что мы 
# можем копировать эти файлы с машин где вручную эти 
# лицензии подтвердили и что автоматически  docker build -t android-build:7.4-30-30 .
# их сгенерировать нельзя docker run --rm -v "%cd%":/home/gradle/ -w /home/gradle android-build:7.4-30-11 gradle assembleDebug

    && mkdir "$ANDROID_HOME/licenses" || true \
    && echo "24333f8a63b6825ea9c5514f83c2829b004d1" > "$ANDROID_HOME/licenses/android-sdk-license" \
    && echo "84831b9409646a918e30573bab4c9c91346d8" > "$ANDROID_HOME/licenses/android-sdk-preview-license"  
# Install Android Build Tool and Libraries
RUN mv "$ANDROID_HOME/cmdline-tools" "$ANDROID_HOME/latest"
RUN mkdir "$ANDROID_HOME/cmdline-tools"
RUN mv "$ANDROID_HOME/latest" "$ANDROID_HOME/cmdline-tools"
RUN yes | $ANDROID_HOME/cmdline-tools/latest/bin/sdkmanager --licenses	
RUN  $ANDROID_HOME/cmdline-tools/latest/bin/sdkmanager --update
RUN  $ANDROID_HOME/cmdline-tools/latest/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS_VERSION}" \
    "platforms;android-${ANDROID_VERSION}" \
    "platform-tools"
